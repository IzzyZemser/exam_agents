import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import log from '@ajar/marker';
import {connect} from './db/mysql-connection';
// import user_router from './modules/user/user-controller';
import {env} from './utils/index'
import {error_handler,not_found} from './middleware/errors-handler';   
const PORT = Number(env('PORT'));
const HOST = env('HOST');
import agent_router from './modules/agent/agent-controller';
import place_router from './modules/place/place-controller';


const app = express();

// middleware
app.use(cors());
app.use(morgan('dev'))

// routing
// app.use('/api/users', user_router);
app.use('/agents', agent_router);
app.use('/places', place_router);

// central error handling
app.use(error_handler);

//when no routes were matched...
app.use('*', not_found);

//start the express api server
(async ()=> {
  await connect()
  await app.listen(PORT,HOST);
  log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log)

