export interface Place {
    name: string,
    hits: number,
    agent_id: number
  }
  
export enum ItemType {
    Human = "Human",
    Android = "Android",
    Sensor = "Sensor",
    Machine = "Machine"
  }
  
  export interface Agent {
    name: string;
    type: ItemType;
    owner?: string;
    status: "pending" | "active" | "retired";
    places?: Place[];
    addPlace: (place: Place) => void;
  }

export interface AgentAndPlace {
    name: string,
    type: ItemType,
    owner?: string,
    status: "pending" | "active" | "retired",
    hits: number,
    agent_id: number
}
  
  
  
  