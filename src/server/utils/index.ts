export const env = (key: string):string => {
    const value = process.env[key];
    if(!value) throw new Error(`Missing: process.env['${key}'].`);
    return value;
}

// no need to use a whole bunch of conditions like that -
// https://github.com/facebook/jest/blob/a028bc13b8cb7d9f10933da67a41e7f52db277dd/packages/jest-get-type/src/index.ts
/**
 * These 5 lines of code will never produce 'unknown'
 * @param {any} value
 * @returns { string } type
 * @author Yariv Gilad
 */
 export function getType(value:any):string {
    const type = typeof value;
    if (type === "number") return Number.isNaN(value) ? "NaN" : "number";
    if (value === null) return "null";
    if (type === "object") return value?.constructor?.name;
    return type || "unknown";
  }