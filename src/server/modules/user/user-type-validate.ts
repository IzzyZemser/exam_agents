import Ajv, {JTDSchemaType} from "ajv/dist/jtd"
const ajv = new Ajv()


export interface User{
    first_name: string
    last_name: string
    email: string
    phone: string
}
export interface UserUpdate{
    first_name?: string
    last_name?: string
    email?: string
    phone?: string
}

const user_schema: JTDSchemaType<User> = {
    properties: {
        first_name: {type: "string"},
        last_name: {type: "string"},
        email: {type: "string"},
        phone: {type: "string"}
    }
}
const user_update_schema: JTDSchemaType<UserUpdate> = {
    optionalProperties: {
        first_name: {type: "string"},
        last_name: {type: "string"},
        email: {type: "string"},
        phone: {type: "string"}
    }
}
// validate_user is a type guard for User - type is inferred from schema type
export const validate_user = ajv.compile(user_schema)
export const validate_user_update = ajv.compile(user_update_schema)