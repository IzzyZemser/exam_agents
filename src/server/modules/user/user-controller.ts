/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route-async-wrapper";
import express, { Request,Response } from "express";
import log from "@ajar/marker";

import {
  User,
  validate_user,
  validate_user_update
} from "./user-type-validate";

import {
  create_user,
  get_all_users,
  paginate_users,
  get_user_by_id,
  update_user_by_id,
  delete_user_by_id
} from './user-service';

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER
router.post("/",raw(async (req:Request, res:Response) => {

    log.obj(req.body, "create a user, req.body:");

    const valid = validate_user(req.body);
    if(!valid && validate_user.errors) throw validate_user.errors[0];

    const [status,output] = await create_user(req.body as User);
    res.status(status).json(output);

  })
);

// GET ALL USERS
router.get("/",raw(async ( _ , res:Response) => {
    const users: User[] = await get_all_users();
    res.status(200).json(users);
  })
);

router.get('/paginate/:page?/:items?', raw( async(req:Request, res:Response)=> {
    log.obj(req.params, "paginate, req.params:");
    let { page = '0' ,items = '10' } = req.params;
    const rows = await paginate_users(page,items);
    res.status(200).json(rows);   
}))


// GETS A SINGLE USER
router.get("/:id",raw(async (req:Request, res:Response):Promise<void> => {
    const user = await get_user_by_id(req.params.id);
    log.yellow('user:'+ user)
    if (!user) {
       res.status(404).json({message:`No user found. with id of ${req.params.id}`});
    } else{
      res.status(200).json(user);
    }
  })
);

// UPDATES A SINGLE USER
router.put("/:id",raw(async (req:Request, res:Response) => { 

    log.obj(req.body, "update a user, req.body:");

    const valid = validate_user_update(req.body);
    if(!valid && validate_user.errors) throw validate_user.errors[0];

    const [status,output] = await update_user_by_id(req.params.id,req.body as User);
    res.status(status).json(output);

}));


// DELETES A USER
router.delete("/:id", raw( async (req:Request, res:Response) => {  
    const [status,output] = await delete_user_by_id(req.params.id);
    res.status(status).json(output);
}));

export default router;
