import log from "@ajar/marker";
import { connection as db } from "../../db/mysql-connection";
import { OkPacket, RowDataPacket } from "mysql2/promise";
import { User } from "./user-type-validate";
import { getType } from "../../utils";

// CREATES A NEW USER
export const create_user = async (
  payload: User
): Promise<[number, { message: string; result: OkPacket }]> => {
  const sql = `INSERT INTO users SET ?`;
  const results = await db.query(sql, payload);
  const result: OkPacket = results[0] as OkPacket;
  const ok = { status: 200, message: `User Created successfully` };
  const fail = { status: 404, message: `Error in creating user ` };
  const { status, message } = result.affectedRows ? ok : fail;
  return [status, { message, result }];
};

// GET ALL USERS
export const get_all_users = async (): Promise<User[]> => {
  // const sql = `
  // SELECT first_name, last_name, email, phone
  // FROM users
  // ORDER BY last_name asc;`;

  const sql = `SELECT * FROM users
               ORDER BY id DESC`;
  const results = await db.query(sql);

  /*  results is an Array. 
        it contains two arrays
        First array contains the actual data - TextRow[]
        Second array contains column definitions - ColumnDefinition[]
        - mysql doesn't expose these types 
          but we can find out their types 
          using our own getType() function :)
     */
  const rows = results[0] as any[]; // TextRow[];
  const defs = results[1] as any[]; // ColumnDefinition[];

  log.cyan("results is an array", getType(results));
  log.cyan("rows / results[0] - is an", getType(results[0]));
  log.cyan("defs / results[1] - is an", getType(results[1]));
  log.cyan("rows is an Array of", getType(rows[0])); // TextRow
  log.cyan("defs is an Array of", getType(defs[0])); // ColumnDefinition
  log.obj( rows[0] ,"rows is an Array of"); // TextRow
//   log.obj( defs[0] ,"defs is an Array of"); // ColumnDefinition

  return rows as User[];
};

export const paginate_users = async (page: string,items: string): Promise<User[]> => {

  const sql = `
    SELECT * 
    FROM users 
    ORDER BY id DESC
    LIMIT ${items} OFFSET ${parseInt(page) * parseInt(items)}`;
  
    // log.yellow(sql);
    const [rows] = await db.query(sql);
    throw new Error('Kaboom!!!')
    return rows as User[];
};

export const get_user_by_id = async (user_id: string): Promise<User | null> => {
  const sql = `SELECT * FROM users WHERE id = '${user_id}'`;
  const results = (await db.query(sql)) as any[];
  const rows = results[0] ; // TextRow[]
  if(!rows[0]) return null;
  return rows[0] as User;
};

export const update_user_by_id = async (
  user_id: string,
  payload: User
): Promise<[number, { message: string; result: OkPacket }]> => {
  const updates = Object.entries(payload).map(([key]) => `${key}=?`);
  const sql = `UPDATE users SET ${updates} WHERE id='${user_id}'`;
  log.yellow(sql);
  //#region
  /*    
    const result = await db.query(
      `UPDATE programming_languages 
      SET first_name=?, last_name=?, email=?, phone=? 
      WHERE id=?`, 
      [ 
        'Roger', 'Rabbit', 'roger@rabbit.io', '+972-54-4869-722',
        'b8ba2a9c-9dc2-11eb-82c9-00e270875516'
      ]
    ); 
*/
  //#endregion

  const results = await db.query(sql, Object.values(payload));
  const result: OkPacket = results[0] as OkPacket;
  const ok = { status: 200, message: `User ${user_id} updated successfully` };
  const fail = { status: 404, message: `Error in updating user ${user_id}` };
  const { status, message } = result.affectedRows ? ok : fail;
  return [status, { message, result }];
};

export const delete_user_by_id = async (
  user_id: string
): Promise<[number, { message: string; result: OkPacket }]> => {
  var sql = `DELETE FROM users WHERE id=?`;
  const results = await db.query(sql, [user_id]);

  const result: OkPacket = results[0] as OkPacket;
  const ok = {
    status: 200,
    message: `User id: ${user_id} - deleted successfully`,
  };
  const fail = { status: 404, message: `User id: ${user_id} - Not Found` };
  const { status, message } = result.affectedRows ? ok : fail;

  return [status, { message, result }];
};
