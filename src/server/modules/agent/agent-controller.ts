import raw from "../../middleware/route-async-wrapper";
import express, { Request,Response } from "express";
import log from "@ajar/marker";

import {create_agent, get_all_agents, get_agent_by_id, get_all_agents_and_places} from './agent-service'
import {Agent} from '../../types/types'
import {validateAgent} from './agent-type-validate'


const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW AGENT
router.post("/",raw(async (req:Request, res:Response) => {
    log.obj(req.body, "create a user, req.body:");
    const valid = validateAgent(req.body);
    if(!valid && validateAgent.errors) throw new Error("not valid agent in post route");
    const { status, message, result } = await create_agent(req.body as Agent);
    res.status(status).json({ message, result });
  })
);

// GET ALL AGENTS
router.get("/",raw(async ( _ , res:Response) => {
    const users: Agent[] = await get_all_agents();
    res.status(200).json(users);
  })
);

// GET ALL AGENTS AND CORESSPONDING PLACES
router.get("/all",raw(async ( req:Request, res:Response) => {
    const agent = await get_all_agents_and_places();
        if (!agent) {
            res.status(404).json({ message: `No agents found in all-agents route` });
        } else {
            res.status(200).json(agent);
        }
    
  })
);

//GET AGENT AND CORESSPONDING PLACES BY ID
router.get("/:id",raw(async ( req:Request, res:Response) => {
    const agent = await get_agent_by_id(req.params.id);
        if (!agent) {
            res.status(404).json({ message: `No agent found with id of ${req.params.id}` });
        } else {
            res.status(200).json(agent);
        }
  })
);


export default router;
