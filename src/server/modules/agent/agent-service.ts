import log from "@ajar/marker";
import { connection as db } from "../../db/mysql-connection";
import { OkPacket, RowDataPacket } from "mysql2/promise";
import { getType } from "../../utils";
import {Agent, AgentAndPlace} from '../../types/types'


// CREATES A NEW AGENT
export const create_agent = async (agent:Agent): Promise<{status:number, message:string, result: OkPacket}> => {
    const sql = `INSERT INTO agents SET ?`;
    const results = await db.query(sql, agent);
    const result:OkPacket = results[0] as OkPacket;
    const {status,message } = result.affectedRows  ? {status:200,message:`User Created successfully`} : {status:404,message:`Error in creating user `};
    return {status,message, result};
}
// GET ALL AGENTS
export const get_all_agents = async (): Promise<Agent[]> => {
    const sql = `SELECT * FROM agents`;
    const [rows] = await db.query(sql);
    return rows as Agent[]
};

export const get_agent_by_id = async (id: string): Promise<AgentAndPlace[]|null> =>  {
    const sql = `SELECT * FROM agents LEFT JOIN places ON places.agent_id = agents.id WHERE agents.id = ${id} `;
    const [results] = await db.query(sql) as any[]
    const club = results[0]
    return !club? null : results as AgentAndPlace[];
}

export const get_all_agents_and_places = async (): Promise<AgentAndPlace[]|null> =>  {
    const sql = `SELECT * FROM agents LEFT JOIN places ON places.agent_id = agents.id`;
    const [results] = await db.query(sql) as any[]
    const club = results[0]
    return !club? null : results as AgentAndPlace[];
}

