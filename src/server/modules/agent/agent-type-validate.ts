import Ajv from "ajv/dist/jtd"
const ajv = new Ajv();
const agentSchema = {
    properties: {
        name: { type: "string" },
        type: { type: "string" },
        status: { type: "string" }
    },
    optionalProperties: {
        owner: { type: "string" }
    }
}


export const validateAgent = ajv.compile(agentSchema);


