import Ajv, {JTDSchemaType} from "ajv/dist/jtd"
import {Place} from '../../types/types'
const ajv = new Ajv();
// const agentSchema: JTDSchemaType<Agent>  = {
const placeSchema: JTDSchemaType<Place> = {
    properties: {
        name: { type: "string" },
        hits: { type: "int32" },
        agent_id: { type: "int32" }
    }
}


export const validatePlace = ajv.compile(placeSchema);
