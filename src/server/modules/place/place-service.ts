import log from "@ajar/marker";
import { connection as db } from "../../db/mysql-connection";
import { OkPacket, RowDataPacket } from "mysql2/promise";
import { getType } from "../../utils";
import {Place} from '../../types/types'


// CREATES A NEW USER
export const create_place = async (agent:Place): Promise<{status:number, message:string, result: OkPacket}> => {
    const sql = `INSERT INTO places SET ?`;
    const results = await db.query(sql, agent);
    const result:OkPacket = results[0] as OkPacket;
    const {status,message } = result.affectedRows  ? {status:200,message:`User Created successfully`} : {status:404,message:`Error in creating user `};
    return {status,message, result};
}

export const get_all_places = async (): Promise<Place[]> => {
    const sql = `SELECT * FROM places`;
    const [rows] = await db.query(sql);
    return rows as Place[]
};
