import raw from "../../middleware/route-async-wrapper";
import express, { Request,Response } from "express";
import log from "@ajar/marker";

import {create_place, get_all_places} from './place-service'
import {Place} from '../../types/types'
import {validatePlace} from './place-type-validate'


const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW place
router.post("/",raw(async (req:Request, res:Response) => {
    log.obj(req.body, "create a user, req.body:");
    const valid = validatePlace(req.body);
    if(!valid && validatePlace.errors) throw new Error("not valid place in post route");
    const { status, message, result } = await create_place(req.body as Place);
    res.status(status).json({ message, result });
  })
);

//get all places
router.get("/",raw(async ( _ , res:Response) => {
    const users: Place[] = await get_all_places();
    res.status(200).json(users);
  })
);

export default router;
